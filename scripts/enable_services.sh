gcloud projects list

project=$(gcloud projects list --filter name='qwiklabs-gcp*' --format='table[no-heading]("PROJECT_ID")')

if [ ! $project ];
then
  echo "ERROR - There appears to be a problem with your Cloud Shell setup. Restart of shell necessary."
  export BADSHELL=TRUE
  exit
fi

gcloud config set project $project 


gcloud services enable secretmanager.googleapis.com 

