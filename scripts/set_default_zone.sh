region=${1::-2}
zone=$1
echo "setting default region $region and zone $zone" 
echo $zone > ~/.lab-zone
gcloud compute project-info add-metadata \
	    --metadata google-compute-default-region=$region,google-compute-default-zone=$zone
gcloud config set compute/zone $zone
