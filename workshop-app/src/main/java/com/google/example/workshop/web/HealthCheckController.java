package com.google.example.workshop.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {
    @RequestMapping("/healthz")
    public String healthz(){
        return "ok";
    }
}