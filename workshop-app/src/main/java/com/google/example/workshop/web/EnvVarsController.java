package com.google.example.workshop.web;

import java.util.Map;
import java.util.Properties;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EnvVarsController{

    @RequestMapping("/env")
    public Map<String, String> env(){
        return System.getenv();
    }

    @RequestMapping("props")
    public Properties props(){
        return System.getProperties();
    }


}