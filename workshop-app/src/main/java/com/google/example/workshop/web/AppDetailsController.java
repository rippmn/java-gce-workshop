package com.google.example.workshop.web;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppDetailsController {
    @RequestMapping("/appDetails")
    public Map<String,String> appDetails() throws Exception{
        Map<String,String> m = new HashMap<String,String>();
        m.put("hostname", InetAddress.getLocalHost().getHostName());
        return m;
    }
}
